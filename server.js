const express = require('express')
const path = require('path')
const fs = require('fs')
const app = express()
const port = 3000
const morgan = require('morgan')
const mongo = require('mongodb').MongoClient
const assert = require('assert')
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

const url = 'mongodb://localhost:27017'
const client = new mongo(url);
const bdName = 'start';


app.use(express.json())

app.set('view engine', 'ejs');

const createPath = (page) => { return path.resolve(__dirname, 'views', `${page}.ejs`) }




//app.use(express.static('public')) //Добавление папки из которой можно передавать данные

//Логгирование запросов 
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

app.get('/', async (req, res) => {
    await client.connect();
    const db = client.db(bdName);
    const collection = db.collection('start');
    const cursor = await collection.find({}).toArray();
    client.close();
    res.render(createPath("index"), { users: cursor })
})




//Проверка ошибок
app.use("/users/change/*", (req, res, next) => {
    const { name, color } = req.body;
    console.log(name, color)
    if (!name || (req.path == '/users/change/add' && !color)) {
        console.log("ytdthyj")
        res.status(400).send("Неправильные данные");
    }
    else
        next();
})



app.post('/users/change/add', async (req, res) => {
    await client.connect();
    const db = client.db(bdName);
    const collection = db.collection('start');
    const cursor = await collection.find({ name: req.body.name }).toArray();
    if (cursor.length != 0) {
        client.close();
        res.status(400).send("Пользователь существует");
    }
    else {
        await collection.insertOne({ name: req.body.name, color: req.body.color })
        client.close();
        res.redirect('/')
    }
})
app.post('/users/change/delete', async (req, res) => {
    await client.connect();
    const db = client.db(bdName);
    const collection = db.collection('start');
    const cursor = await collection.find({ name: req.body.name }).toArray();
    if (cursor.length == 0) {
        client.close();
        res.status(400).send("Пользователь не существует");
    }
    else {
        await collection.deleteOne({name:req.body.name})
        client.close();
        res.redirect('/')
    }
})
app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})

